package com.mall4j.cloud.search.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author FrozenWatermelon
 * @date 2020/9/24
 */
@Data
@Component
@ConfigurationProperties(prefix = "elastic")
public class ElasticProperties {

    private String address;
    private int port;
    private String account;
    private String password;
    private String scheme;
}
